
const controller = require("../controllers/product.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/product", controller.getData);
  app.post("/api/test/add/product", controller.addData);
  app.put("/api/test/update/product", controller.updateData);
  app.delete("/api/test/del/product/:id", controller.deleteData);


};

