const controller = require("../controllers/vendor.controller");


module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/vendor", controller.getData);
  app.post("/api/test/add/vendor", controller.addData);
  app.put("/api/test/update/vendor", controller.updateData);
  app.delete("/api/test/del/vendor/:id", controller.deleteData);


};