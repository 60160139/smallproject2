
const controller = require("../controllers/customer.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/customers", controller.getData);
  app.post("/api/test/add/customers", controller.addData);
  app.put("/api/test/update/customers", controller.updateData);
  app.delete("/api/test/del/customers/:id", controller.deleteData);
  
};
