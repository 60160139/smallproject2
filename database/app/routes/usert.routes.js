
const controller = require("../controllers/usert.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/usert", controller.getData);
  app.post("/api/test/add/usert", controller.addData);
  app.put("/api/test/update/usert", controller.updateData);
  app.delete("/api/test/del/usert/:id", controller.deleteData);
  
};
