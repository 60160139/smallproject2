const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.product = require("./product.model");
db.customer = require("./customer.model");
db.usert = require("./usert.model");
db.vendor = require("./vendor.model");
db.ROLES = ["user", "admin", "moderator"];

module.exports = db;