const mongoose = require("mongoose");

const Vendor = mongoose.model(
  "Vendor",
  new mongoose.Schema({
    name: String,
    company: String,
    Tel: Number
  })
  
  );
  module.exports = Vendor;
