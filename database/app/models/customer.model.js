const mongoose = require("mongoose");

const Customer = mongoose.model(
  "Customer",
  new mongoose.Schema({
  name: String,
  address: String,
  dob: String,
  gender: String,
  mobile: String,
  email: String
    
  })
);

module.exports = Customer;